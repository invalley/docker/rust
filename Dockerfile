FROM localhost:5000/rockylinux:9.0.0.6cf606e9 as build

ARG SNYK_TOKEN=""

ENV PATH="/rust/.cargo/bin:$PATH"

RUN microdnf install tar wget sudo -y
COPY files/sudoers /etc/sudoers

RUN curl https://static.snyk.io/cli/latest/snyk-linux -o /tmp/snyk && chmod +x /tmp/snyk
RUN /tmp/snyk config set api=${SNYK_TOKEN}

RUN microdnf install epel-release cmake gcc make curl clang -y && microdnf clean all

RUN adduser rust

USER rust

WORKDIR /tmp

RUN curl -O https://static.rust-lang.org/rustup/dist/x86_64-unknown-linux-gnu/rustup-init && chmod +x rustup-init && \
    ./rustup-init -y